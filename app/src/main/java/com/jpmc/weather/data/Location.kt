package com.jpmc.weather.data

/*
 * Data class to hold Location Data. Modeled from https://openweathermap.org/api/geocoding-api .
 * Excluded local_names since I will only be displaying in English.
 */
data class Location(
    val country: String = "",
    val lat: Double,
    val lon: Double,
    val name: String = "",
    val state: String = "",
    val zip: String = ""
) {
    override fun equals(other: Any?): Boolean {
        if( other !is Location) {
            return false
        }
        if (Math.abs(lat - other.lat) < .5 && Math.abs(lon - other.lon) < .5) {
            return true
        }

        return name == other.name && state == other.state
    }
}