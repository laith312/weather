package com.jpmc.weather.data

data class Rain(
    val `1h`: Double
)