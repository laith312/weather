package com.jpmc.weather.repository;

import com.jpmc.weather.network.OpenWeatherMapAPI;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This class is not a true Repository. I did this to save time. Normally we should cache
 * data in a DB using Room, however I didnt have time to add Room.
 * The Repo should be designed to check if DB data is stale before fetching from network.
 * <p>
 * This should be injected into the viewmodels
 */
public class OpenWeatherRepository {
   private static final String BASE_URL = "https://api.openweathermap.org/";

   public static OpenWeatherMapAPI instance =
           new Retrofit.Builder()
                   .baseUrl(BASE_URL)
                   .client(buildOkHttpClient())
                   .addConverterFactory(GsonConverterFactory.create())
                   .build().create(OpenWeatherMapAPI.class);

   private static OkHttpClient buildOkHttpClient() {
      return new OkHttpClient.Builder()
              .build();
   }
}

