package com.jpmc.weather.ui

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.jpmc.weather.data.Location
import com.jpmc.weather.ui.screen.Screen
import com.jpmc.weather.ui.screen.home.HomeScreenComposable
import com.jpmc.weather.ui.screen.search.SearchScreenComposable

@Composable
fun MainActivityNavigation(defaultLocation: Location?) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Screen.MainScreen.route) {
        composable(route = Screen.MainScreen.route) {
            HomeScreenComposable(navController = navController, defaultLocation)
        }
        composable(route = Screen.SearchScreen.route) {
            SearchScreenComposable(navController = navController)
        }
    }
}